# Architecture

Prometheus Alias Server requires the following components to function:

- At least one client that is compatible with PAS
- A server to receive scrapes requests from and send metrics to
- An externally hosted database for identification and authentication of aliases and their real-world mappings
