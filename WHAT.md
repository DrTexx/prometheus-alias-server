# What is Prometheus Alias Server

Prometheus Alias Server (hereby referred to as 'PAS') is a simple, scalable service that proxies prometheus scrape requests through a secure server without the requirement to expose ports or mess with Prometheus' pull-based scraping architecture
